# This plugin was moved

The BOXdash plugin was moved away from GitLab. The new location can be found here:

* <https://git.0x7be.net/dirk/boxdash>

Just update your plugin manager configuration.

```lua
-- Use packer, for exaple:
require('packer').startup(function(use)
    use 'https://git.0x7be.net/dirk/boxdash'
end
```

So basically all `https://gitlab.com/4w/neovim-boxdash` become `https://git.0x7be.net/dirk/boxdash`

The functionality of the plugin did not change. It is still in active development and changes or fixes are made when needed. All that has changed is the public Git hosting location.
